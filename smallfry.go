package main

import (
	"io/ioutil"
	"strings"
	"errors"
	"net/http"
	"fmt"
	"log"
	"html/template"
)

// Defines a type for an entry that is rendered on a page
type Entry struct {
	Author string
	Date string
	Title string
	Body []string
}

// Defines a type for each item on the list of posts rendered on the blog index
type IndexItem struct {
	Title string
	Date string
	Slug string
}

// Loads a blog post from a file, using a slug. Also checks to ensure that the post
// is not malformed, and contains all of the necessary metadata (Author, Date and Title)
func loadentry(slug string) (*Entry, error) {
	filename := "./blog/" + slug + ".txt"
	raw, rerr := ioutil.ReadFile(filename)
	if rerr != nil {
		return nil, rerr
	}

	content := strings.SplitN(string(raw), "\n", 4)
	if len(content) > 3 {
		entry := &Entry{}
		for i := 0; i < 3; i += 1 {
			this := strings.Split(content[i], ":")
			switch this[0] {
			case "Author":
				entry.Author = strings.Trim(this[1], " ")
			case "Date":
				entry.Date = strings.Trim(this[1], " ")
			case "Title":
				entry.Title = strings.Trim(this[1], " ")
			default:
				return nil, errors.New("Malformed Post")
			}
		}
		entry.Body = strings.Split(content[3], "\n\n")
		return entry, nil
	} else {
		return nil, errors.New("Malformed Post")
	}
}

// Loads the index file, and puts all that information in a slice of the
// IndexItem type. If the list is empty, it ensures that an empty slice
// is returned.
func loadindex() ([]IndexItem, error) {
	raw, rerr := ioutil.ReadFile("./blog/postindex.txt")
	if rerr != nil {
		return nil, rerr
	}
	posts := strings.Split(string(raw), "\n")
	items := make([]IndexItem, 0, len(posts))
	if len(posts) > 1 {
		for i := len(posts) - 1; i >= 0; i -= 1 {
			this := strings.Split(posts[i], ":")
			items = append(items, IndexItem{
				Date:  strings.Trim(this[0], " "),
				Title: strings.Trim(this[1], " "),
				Slug:  strings.Trim(this[2], " "),
			})
		}
	}
	return items, nil
}

// handles requests to the "/blog/" path, if there is a sub URL on that path
// it renders the requested post, if there isn't, it shows the index page.
// This function also handles reporting errors to viewers.
func handleblog(w http.ResponseWriter, r *http.Request) {
	slug := r.URL.Path[len("/blog/"):]
	if slug == "" || slug == "postindex" {
		posts, perr := loadindex()
		if perr != nil {
			fmt.Fprintf(w, "500 Error: Could not read post index file")
		} else {
			t, terr := template.ParseFiles("./views/blogindex.html")
			if terr != nil {
				fmt.Fprintf(w, "500 Error: Could not read the required template file")
			} else {
				t.Execute(w, posts)
			}
		}
	} else {
		entry, berr := loadentry(slug)
		if berr != nil {
			fmt.Fprintf(w, "500 Error: Could not find requested blog post, or post is malformed")
		} else {
			t, terr := template.ParseFiles("./views/post.html")
			if terr != nil {
				fmt.Fprintf(w, "500 Error: Could not read the required template file")
			} else {
				t.Execute(w, entry)
			}
		}
	}
}

func main() {
	http.HandleFunc("/blog/", handleblog)
	fs := http.FileServer(http.Dir("www"))
	http.Handle("/", fs)
	log.Fatal(http.ListenAndServe(":8080", nil))
}